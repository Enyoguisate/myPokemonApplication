﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls.WebParts;
using myPokemonApp.Model;

namespace myPokemonAplpication.DAL
{
    public class PokemonDAL : Database
    {
        public PokemonDAL() : base()
        {

        }

        public List<Pokemon> GetAllPokemon()
        {
            try
            {
                using (var cmd = GetCommand("[SP_GET_POKEMON_BY_ID]", CommandType.StoredProcedure))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                            return null;
                        //determine the ordinal for each of the columns in the reader response.
                        int idxId = reader.GetOrdinal("Id");
                        int idxName = reader.GetOrdinal("name");
                        int idxType = reader.GetOrdinal("type");
                        int idxUrlImage = reader.GetOrdinal("urlImage");
                        int idxPreviousEvolution = reader.GetOrdinal("previousEvolution");
                        int idxNextEvolution = reader.GetOrdinal("nextEvolution");

                        List<Pokemon> pokemonList = new List<Pokemon>();
                        while (reader.Read()) //iterate along the reader 
                        {
                            //create object to add to collection
                            Pokemon pokemon = new Pokemon();
                            pokemon.Id = reader.GetInt32(idxId);
                            PokemonType type;
                            PokemonType.TryParse(reader.GetString(idxType), out type);
                            pokemon.Type = type;
                            pokemon.UrlImage = reader.GetString(idxUrlImage);
                            pokemon.NextEvolution = reader.GetInt32(idxNextEvolution);
                            pokemon.PreviousEvolution = reader.GetInt32(idxPreviousEvolution);
                            pokemon.Name = reader.GetString(idxName);

                            //add new pokemon to collection
                            pokemonList.Add(pokemon);

                        }
                        return pokemonList;
                    }
                }
            }
            catch (Exception)
            {
                //IF ERROR RAISE THE ERROR TO THE UPPER LAYER
                throw;
            }
            finally
            {
                //ALWAYS CLOSE THE CONNECTION AT THE END OF EACH METHOD
                CloseConnection();
            }

        }

        public bool InsertPokemon(Pokemon aPokemon)
        {
            bool aState = false;
            var id = GetInsertId("Id", "pokemon");
            try
            {
                using (var cmd = GetCommand("[SP_INS_POKEMON]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Parameters.AddWithValue("@Name", aPokemon.Name);
                    cmd.Parameters.AddWithValue("@Type", aPokemon.Type);
                    cmd.Parameters.AddWithValue("@UrlImage", aPokemon.UrlImage);
                    cmd.Parameters.AddWithValue("@previousevolution",
                        aPokemon.PreviousEvolution == 0 ? null : aPokemon.PreviousEvolution);
                    cmd.Parameters.AddWithValue("@NextEvolution",
                        aPokemon.NextEvolution == 0 ? null : aPokemon.NextEvolution);
                    cmd.ExecuteNonQuery();
                    aState = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }



        public List<Pokemon> GetPokemonById(int id)
        {
            List<Pokemon> pokemonList = new List<Pokemon>();
            try
            {
                using (var cmd = GetCommand("[SP_GET_POKEMON_BY_ID] @id={0}", CommandType.StoredProcedure))
                {
                    //falta agregar el parametro al sp osea el Id
                    //SqlParameter inParam = new SqlParameter("@id", SqlDbType.Int);
                    //inParam.ParameterName = "@id";
                    //inParam.Direction = ParameterDirection.Input;
                    //inParam.Value = id;
                    //cmd.Parameters.Add(inParam);
                    cmd.Parameters.AddWithValue("@id", SqlDbType.Int);
                    //cmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id");
                    //cmd.Parameters("@Id").Direction = ParameterDirection.Output;
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows) return null;
                        int idxId = reader.GetOrdinal("Id");
                        int namexName = reader.GetOrdinal("Name");
                        int typexType = reader.GetOrdinal("Type");
                        int previousEvolutionxPreviousEvolution = reader.GetOrdinal("PreviousEvolution");
                        int nextEvolutionxNextEvolution = reader.GetOrdinal("NextEvolution");
                        int idxUrlImage = reader.GetOrdinal("urlImage");

                        while (reader.Read())
                        {
                            Pokemon pokemon = new Pokemon();
                            pokemon.Id = reader.GetInt32(idxId);
                            PokemonType type;
                            PokemonType.TryParse(reader.GetString(typexType), out type);
                            pokemon.Type = type;
                            pokemon.UrlImage = reader.GetString(idxUrlImage);
                            pokemon.NextEvolution = reader.GetInt32(nextEvolutionxNextEvolution);
                            pokemon.PreviousEvolution = reader.GetInt32(previousEvolutionxPreviousEvolution);
                            pokemon.Name = reader.GetString(namexName);

                            pokemonList.Add(pokemon);
                        }
                        return pokemonList;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool UpdPokemon(Pokemon aPokemon)
        {
            var aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_UPD_POKEMON] @id=" + aPokemon.Id, CommandType.StoredProcedure))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                            return aState;
                        cmd.Parameters.AddWithValue("@Id", aPokemon.Id);
                        cmd.Parameters.AddWithValue("@Name", aPokemon.Name);
                        cmd.Parameters.AddWithValue("@Type", aPokemon.Type);
                        cmd.Parameters.AddWithValue("@UrlImage", aPokemon.UrlImage);
                        cmd.Parameters.AddWithValue("@previousevolution",
                            aPokemon.PreviousEvolution == 0 ? null : aPokemon.PreviousEvolution);
                        cmd.Parameters.AddWithValue("@NextEvolution",
                            aPokemon.NextEvolution == 0 ? null : aPokemon.NextEvolution);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            aState = true;
                        }
                    }
                }
                return aState;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool ValExist(Pokemon aPokemon)
        {
            bool aState = false;
            List<Pokemon> pokemonList = GetPokemonById(aPokemon.Id);
            if (pokemonList.Count == 0)
            {
                return aState;
            }
            else
            {
                aState = true;
            }
            return aState;
        }
    }
}
