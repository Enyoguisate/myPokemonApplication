﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using myPokemonApp.Model;

namespace myPokemonAplpication.DAL
{
    class UserDAL:Database
    {
        public UserDAL() : base()
        {
            
        }

        public List<User> GetUser()
        {
            try
            {
                using (var cmd = GetCommand("[SP_GET_USER]", CommandType.StoredProcedure))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                            return null;
                        int userNamexUserName = reader.GetOrdinal("UserName");
                        int passwordxPassword = reader.GetOrdinal("Password");
                        int fnamexFname = reader.GetOrdinal("Fname");
                        int lnamexLname = reader.GetOrdinal("Lname");
                        int areaCodexAreaCode = reader.GetOrdinal("AreaCode");
                        int phonexPhone = reader.GetOrdinal("Phone");
                        int birthDayxBirthDay = reader.GetOrdinal("BirthDay");
                        int emailxEmail = reader.GetOrdinal("Email");
                        int trainerLvlxTrainerLvl = reader.GetOrdinal("TrainerLvl");
                        //int rolidxRolId = reader.GetOrdinal("RolId");

                        List<User> userList = new List<User>();
                        while (reader.Read())
                        {
                            User user = new User();
                            user.UserName = reader.GetString(userNamexUserName);
                            user.Password = reader.GetString(passwordxPassword);
                            user.Fname = reader.GetString(fnamexFname);
                            user.Lname = reader.GetString(lnamexLname);
                            user.AreaCode = reader.GetInt32(areaCodexAreaCode);
                            user.Phone = reader.GetInt32(phonexPhone);
                            user.BirthDay = reader.GetDateTime(birthDayxBirthDay);
                            user.Email = reader.GetString(emailxEmail);
                            user.TrainerLvl = reader.GetInt32(trainerLvlxTrainerLvl);

                            userList.Add(user);
                        }

                        return userList;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool InsertUser(User user)
        {
            bool aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_INS_USER]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@UserName", user.UserName);
                    cmd.Parameters.AddWithValue("@Password", user.Password);
                    cmd.Parameters.AddWithValue("@Fname", user.Fname);
                    cmd.Parameters.AddWithValue("@Lname", user.Lname);
                    cmd.Parameters.AddWithValue("@AreaCode", user.AreaCode);
                    cmd.Parameters.AddWithValue("@Phone", user.Phone);
                    cmd.Parameters.AddWithValue("@BirthDay", user.BirthDay);
                    cmd.Parameters.AddWithValue("@Email", user.Email);
                    cmd.Parameters.AddWithValue("@TrainerLvl", user.TrainerLvl);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }
    }
    //public bool UpdateUser(User user)
    //{
    //    var aState = false;
    //    try
    //    {
    //        using (var cmd = GetCommand("[SP_UPD_USERS]", CommandType.StoredProcedure))
    //        {
    //            using (var reader = cmd.ExecuteReader())
    //            {
    //                if (!reader.HasRows())
    //                    return aState;

    //                int userNamexUserName = reader.GetOrdinal(“UserName”);
    //                int passwordxPassword = reader.GetOrdinal(“Password”);
    //                int fnamexFname = reader.GetOrdinal(“Fname”);
    //                int lnamexLname = reader.GetOrdinal(“Lname”);
    //                int areacodexAreaCode = reader.GetOrdinal(“AreaCode”);
    //                int phonexPhone = reader.GetOrdinal(“Phone”);
    //                int birthdayxBirthDay = reader.GetOrdinal(“BirthDay”);
    //                int emailxEmail = reader.GetOrdinal(“Email”);
    //                int trainerlvlxTrainerLvl = reader.GetOrdinal(“TrainerLvl”);

    //                cmd.Parameters.AddWithValue(“@Name”, user.Name);
    //                cmd.Parameters.AddWithValue(“@Password”, user.Password);
    //                cmd.Parameters.AddWithValue(“@Fname”, user.Fname);
    //                cmd.Parameters.AddWithValue(“@Lname”, user.Lname);
    //                cmd.Parameters.AddWithValue(“@AreaCode”, user.AreaCode);
    //                cmd.Parameters.AddWithValue(“@Phone”, user.Phone);
    //                cmd.Parameters.AddWithValue(“@BirthDay”, user.BirthDay);
    //                cmd.Parameters.AddWithValue(“@Email”, user.Email);
    //                cmd.Parameters.AddWithValue(“@TrainerLvl”, user.TrainerLvl);

    //                if (cmd.ExecuteNonQuery() > 0)
    //                {
    //                    aState = true;
    //                }
    //            }
    //        }
    //        return aState;
    //    }
}
