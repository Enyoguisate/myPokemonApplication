﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using myPokemonApp.Model;
using Microsoft.Win32;

namespace MVC.DAL
{
    // THIS CALL WAS CREATED INSIDE OF A WEB APLICATION PROJECT. ALL PROJECT THAT DOESN'T HAVE ACCESS FROM A WEB NEEDS TO BE LIBRARY CLASS
    // BY CREATING A WEB PROJECT THE REFERENCE BETWEEN THIS PROJECT AND myPokemonAplication NEEDS TO BE CREATED USING A DIFERENT APROACH.
    // ALSO BY CREATING THIS  AS A WEB PROJECT YOU ARE USING REFERENCES INSIDE THE PROJECT THAT WILL AFFECT THE PERFORMANCE AND THE DEFINITION OF CODE-REUSE.
    public class myAppDal
    {
        private readonly SqlConnection myDbConection = new SqlConnection(WebConfigurationManager.ConnectionStrings["myDbConection"].ConnectionString);
        public List<Pokemon> GetPokemon(int id)
        {
            var pokeList = new List<Pokemon>();
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand("SP_GET_POKEMON_BY_ID", myDbConection))
                {
                    myDbConection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var pokemon = new Pokemon
                        {
                            Id = Convert.ToInt32(reader[0]),
                            Name = reader[1].ToString(),
                            Type = (PokemonType)reader[2],
                            UrlImage = reader[3].ToString(),
                            PreviousEvolution = Convert.ToInt32(reader[4]),
                            NextEvolution = Convert.ToInt32(reader[5]),
                        };
                        pokeList.Add(pokemon);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return pokeList;
        }
        public bool UpdatePokemon(Pokemon pokemon)
        {
            bool aState = false;
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand("SP_UPD_POKEMON", myDbConection))
                {
                    myDbConection.Open();
                    //id=@id???
                    var query = new SqlCommand("UPDATE Pokemon SET Id=@id, Name=@name, Type=@type, " +
                                               "UrlImage=@utlImage, PreviousEvolution=@previousEvolution," +
                                               "NextEvolution=@nextEvolution", myDbConection);
                    
                    query.Parameters.AddWithValue("@name", pokemon.Name);
                    query.Parameters.AddWithValue("@type", pokemon.Type);
                    query.Parameters.AddWithValue("@urlImage", pokemon.UrlImage);
                    query.Parameters.AddWithValue("@previousEvolution", pokemon.PreviousEvolution);
                    query.Parameters.AddWithValue("@nextEvolution", pokemon.NextEvolution);
                    query.ExecuteNonQuery();
                    aState = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return aState;
        }
        public bool InsertPokemon(Pokemon pokemon)
        {
            bool aState = false;
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand("SP_INS_POKEMON", myDbConection))
                {
                    myDbConection.Open();
                    var query = new SqlCommand("INSERT INTO Pokemon(id,name,type,urlImage,previousEvolution,nextEvolution) VALUES (@id, @name, @type, @urlImage, @previousEvolution, @nextEvolution");
                    query.Parameters.AddWithValue("@Id", pokemon.Id);
                    query.Parameters.AddWithValue("@name", pokemon.Name);
                    query.Parameters.AddWithValue("@type", pokemon.Type);
                    query.Parameters.AddWithValue("@urlImage", pokemon.UrlImage);
                    query.Parameters.AddWithValue("@previousEvolution", pokemon.PreviousEvolution);
                    query.Parameters.AddWithValue("@nextEvolution", pokemon.NextEvolution);
                    query.ExecuteNonQuery();
                    aState = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return aState;
        }
    }
}




