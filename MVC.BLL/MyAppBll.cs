﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC.DAL;
using myPokemonApp.Model;
namespace MVC.BLL
{
    public class MyAppBll
    {
        // THIS CALL WAS CREATED INSIDE OF A WEB APLICATION PROJECT. ALL PROJECT THAT DOESN'T HAVE ACCESS FROM A WEB NEEDS TO BE LIBRARY CLASS
        // BY CREATING A WEB PROJECT THE REFERENCE BETWEEN THIS PROJECT AND myPokemonAplication NEEDS TO BE CREATED USING A DIFERENT APROACH.
        // ALSO BY CREATING THIS  AS A WEB PROJECT YOU ARE USING REFERENCES INSIDE THE PROJECT THAT WILL AFFECT THE PERFORMANCE AND THE DEFINITION OF CODE-REUSE.


        private myAppDal pokemonDAL = new myAppDal();
        public Pokemon GetPokemon(int id)
        {
            Pokemon aPokemon = new Pokemon();
            foreach (var key in pokemonDAL.GetPokemon(id))
            {
                if (key.Id != 0)
                {
                    aPokemon = key;
                }
                else
                {
                    aPokemon = null;
                }
            }
            return aPokemon;
        }
        public bool UpdatePokemon(Pokemon pokemon)
        {
            return pokemonDAL.UpdatePokemon(pokemon);
        }
        public bool InsertPokemon(Pokemon pokemon)
        {
            return pokemonDAL.InsertPokemon(pokemon);
        }
    }
}

