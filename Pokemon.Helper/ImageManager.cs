﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using Pokemon.Helper;
using System.Net.Mime;
using System.Runtime.Remoting.Messaging;
using System.Web;


namespace Pokemon.Helper
{
    public class ImageManager
    {
        
        public string PathImageFolder { get; set; }
        public string ParentPathImageFolder { get; set; }
        public static  string PathTemporaryFolder { get; set; }
        public string NewFolderName { get; set; }

        public void CopyToTemporaryFolder(string pathFilename,string pathTempFolder)
        {
            var fileN=Path.GetFileName(pathFilename);
            var destinationDir = Path.Combine(pathTempFolder, fileN);
            var originDir = pathFilename;
            File.Copy(originDir,destinationDir);

        }

        public string CreateFolderOfProcessPokemon(string processImageFolder, string imageName)
        {
            var newFolderName=Path.Combine(processImageFolder, Path.GetFileNameWithoutExtension(imageName));
            Directory.CreateDirectory(newFolderName);
            return newFolderName;
        }

        public void DeleteTemporaryFolder(string temporaryFolderPath)
        {
            Directory.Delete(temporaryFolderPath);
        }

        public string CreateTemporaryFolder(string pathToTemporaryFolder, string folderName)
        {
            var destiny = Path.Combine(pathToTemporaryFolder, folderName);
            Directory.CreateDirectory(destiny);
            return destiny;
        }

        public string CreateDirectoryOfProcessedImage(string outPutparentFolder,string folderName)
        {
            var destiny = Path.Combine(outPutparentFolder, folderName);
            Directory.CreateDirectory(destiny);
            return destiny;
        }

        public string GetDestiny(string outPutparentFolder, string folderName)
        {
            return Path.Combine(outPutparentFolder, folderName);
        }

        public string NameImage(string pathFileName,int size)
        {
            var fileName = Path.GetFileNameWithoutExtension(pathFileName);
            var fileExt = Path.GetExtension(Path.GetFileName(pathFileName));
            var res = "";
            if (size == 0)
            {
                res = fileName + "_small." + fileExt;
            }else if (size == 1)
            {
                res = fileName + "_medium." + fileExt;
            }
            else if (size == 2)
            {
                res = fileName + "_large." + fileExt;
            }
            return res;
        }

        public void ListenQueuer()
        {
            var tempFolder = PathTemporaryFolder;
                //"~/myPokemonAplication/App_Data/TempImages/";
            foreach (string d in Directory.GetFiles(tempFolder))
                {
                    if (d.Contains(".toDelete"))
                    {
                        Console.WriteLine(d);
                    }
                }
            

        }

        public void SaveImg(Image img, string folderDest, string fileName)
        {
            Image newImage = img;
            newImage.Save(Path.Combine(folderDest, fileName));
        }
        /// <summary>
        /// Scales an image proportionally.  Returns a bitmap.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="maxWidth"></param>
        /// <param name="maxHeight"></param>
        /// <returns></returns>
        public Bitmap ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            Bitmap bmp = new Bitmap(newImage);

            return bmp;
        }

    }
}
