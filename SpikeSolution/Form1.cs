﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Pokemon.Helper;


namespace SpikeSolution
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            _txtBox_FileToUpload.Text = @"C:\Users\Enyoguisate\Desktop\imagenes pokemons\007-Squirtle.png";
            _txtBox_TemporaryFolerLocation.Text = @"C:\Users\Enyoguisate\Desktop\TemporaryFolder";
            _txtBox_OutputParentFolder.Text = @"C:\Users\Enyoguisate\Desktop\ProcessImages";
            _txtBox_CreateFolderName.Text = @"Squirtle";
        }
        private void _btn_OutputParentFolder_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                _txtBox_OutputParentFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        private void _btn_StartImageProcesing_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_txtBox_CreateFolderName.Text))
                return;

            if (Directory.Exists(_txtBox_TemporaryFolerLocation.Text))
                Directory.Delete(_txtBox_TemporaryFolerLocation.Text, true);

            Directory.CreateDirectory(_txtBox_TemporaryFolerLocation.Text);

            ImageManager manager = new ImageManager();

            manager.CopyToTemporaryFolder(_txtBox_FileToUpload.Text,_txtBox_TemporaryFolerLocation.Text);
            //get img name and ext (img.ext)
            //var pokeName = Path.GetFileName(_txtBox_FileToUpload.Text);
            //add the name of img to temporary destination folder
            //var destinationDir = _txtBox_TemporaryFolerLocation.Text + "\\" + pokeName;//+ "\\000-Squirtle.jpeg";
            //get origin folder
            //var originDir = _txtBox_FileToUpload.Text;
            //copy file to temporary folder
            //File.Copy(originDir, destinationDir);
            //define new ImageManager Object
            //ImageManager manager = new ImageManager();

            //send path to output folder and img folder to ImageManager

            manager.ParentPathImageFolder = _txtBox_OutputParentFolder.Text;
            manager.PathImageFolder = _txtBox_CreateFolderName.Text;
            manager.PathTemporaryFolder = _txtBox_TemporaryFolerLocation.Text;
            manager.NewFolderName = _txtBox_CreateFolderName.Text;

            //set name especifier
            //var pokeName1 = Path.GetFileNameWithoutExtension(_txtBox_FileToUpload.Text) + "_small.png";
            //var pokeName2 = Path.GetFileNameWithoutExtension(_txtBox_FileToUpload.Text) + "_medium.png";
            //var pokeName3 = Path.GetFileNameWithoutExtension(_txtBox_FileToUpload.Text) + "_large.png";
            //name of the final destination folder of image
            //var finalDestiny = Path.Combine(_txtBox_OutputParentFolder.Text, _txtBox_CreateFolderName.Text);
            //create directoy must be in imageManager and must return the path
            //of newly created folder, and the images must be saved by imageManager
            manager.CreateDirectoryOfProcessedImage(_txtBox_OutputParentFolder.Text, _txtBox_CreateFolderName.Text);
            //manager.GetDestiny(_txtBox_OutputParentFolder.Text, _txtBox_CreateFolderName.Text);
            //Directory.CreateDirectory(finalDestiny);
            
            using (Stream reader = File.Open(_txtBox_FileToUpload.Text, FileMode.Open))
            {
                Image img = Image.FromStream(reader);
                //manager.ScaleImage(img,75,75).Save(@"C:\asd.png");
                //manager.ScaleImage(img, 75, 75).Save(Path.Combine(_txtBox_OutputParentFolder.Text,_txtBox_FileToUpload.Text));
                //scale images 
                manager.SaveImg(manager.ScaleImage(img,75,75),manager.GetDestiny(_txtBox_OutputParentFolder.Text,_txtBox_CreateFolderName.Text),manager.NameImage(_txtBox_FileToUpload.Text,0));
                manager.SaveImg(manager.ScaleImage(img, 150, 150), manager.GetDestiny(_txtBox_OutputParentFolder.Text, _txtBox_CreateFolderName.Text), manager.NameImage(_txtBox_FileToUpload.Text, 1));
                manager.SaveImg(img, manager.GetDestiny(_txtBox_OutputParentFolder.Text, _txtBox_CreateFolderName.Text), manager.NameImage(_txtBox_FileToUpload.Text, 2));
                //manager.ScaleImage(img, 75, 75).Save(finalDestiny + "\\" + pokeName1);
                //manager.ScaleImage(img, 150, 150).Save(finalDestiny + "\\" + pokeName2);
                //img.Save(finalDestiny + "\\" + pokeName3);
            }

            //Creacion del Objeto ImageManager 
            //Seteo de propiedades
            //Llamar al metodo que procesa la imagen, primer paso de ese metodo es Crear la carpeta dentro de la ruta de 
            //process images y realizar una copia del archivo que se paso como archivo temporal a esa carpeta.
            //procesar la imagen para crear 3 tamaños 75x75 150x150 y original, creando un file por cada imagen de la forma
            //[imageName]_small.[extension] 
            //[imageName]_medium.[extension] 
            //[imageName]_large.[extension]  ---> esta es la original.
        }

        private void _btn_TemporaryFolderLocation_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                _txtBox_TemporaryFolerLocation.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void _btn_searchFileToUpload_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            //dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            dialog.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|All files(*.*)|(*.*)";
            dialog.InitialDirectory = @"C:\";
            dialog.Multiselect = false;
            dialog.Title = "Please select an image file.";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                _txtBox_FileToUpload.Text = Path.Combine(dialog.InitialDirectory, dialog.FileName);
                //Encrypt the selected file. I'll do this later. :)
            }
            //
            // This event handler was created by double-clicking the window in the designer.
            // It runs on the program's startup routine.
            //

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}

