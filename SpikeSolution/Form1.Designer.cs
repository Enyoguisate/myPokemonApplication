﻿namespace SpikeSolution
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._txtBox_CreateFolderName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._btn_StartImageProcesing = new System.Windows.Forms.Button();
            this._btn_OutputParentFolder = new System.Windows.Forms.Button();
            this._txtBox_OutputParentFolder = new System.Windows.Forms.TextBox();
            this._btn_TemporaryFolderLocation = new System.Windows.Forms.Button();
            this._txtBox_TemporaryFolerLocation = new System.Windows.Forms.TextBox();
            this._btn_searchFileToUpload = new System.Windows.Forms.Button();
            this._txtBox_FileToUpload = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._txtBox_OutputResult = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._txtBox_CreateFolderName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._btn_StartImageProcesing);
            this.groupBox1.Controls.Add(this._btn_OutputParentFolder);
            this.groupBox1.Controls.Add(this._txtBox_OutputParentFolder);
            this.groupBox1.Controls.Add(this._btn_TemporaryFolderLocation);
            this.groupBox1.Controls.Add(this._txtBox_TemporaryFolerLocation);
            this.groupBox1.Controls.Add(this._btn_searchFileToUpload);
            this.groupBox1.Controls.Add(this._txtBox_FileToUpload);
            this.groupBox1.Location = new System.Drawing.Point(32, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 271);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FileSelector";
            // 
            // _txtBox_CreateFolderName
            // 
            this._txtBox_CreateFolderName.Location = new System.Drawing.Point(7, 173);
            this._txtBox_CreateFolderName.Name = "_txtBox_CreateFolderName";
            this._txtBox_CreateFolderName.Size = new System.Drawing.Size(360, 20);
            this._txtBox_CreateFolderName.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Create Folder Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Output Parent Folder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Temporary Folder Location";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "File to Upload";
            // 
            // _btn_StartImageProcesing
            // 
            this._btn_StartImageProcesing.Location = new System.Drawing.Point(60, 208);
            this._btn_StartImageProcesing.Name = "_btn_StartImageProcesing";
            this._btn_StartImageProcesing.Size = new System.Drawing.Size(272, 55);
            this._btn_StartImageProcesing.TabIndex = 8;
            this._btn_StartImageProcesing.Text = "Start Image Procesing";
            this._btn_StartImageProcesing.UseVisualStyleBackColor = true;
            this._btn_StartImageProcesing.Click += new System.EventHandler(this._btn_StartImageProcesing_Click);
            // 
            // _btn_OutputParentFolder
            // 
            this._btn_OutputParentFolder.Location = new System.Drawing.Point(367, 121);
            this._btn_OutputParentFolder.Name = "_btn_OutputParentFolder";
            this._btn_OutputParentFolder.Size = new System.Drawing.Size(36, 23);
            this._btn_OutputParentFolder.TabIndex = 7;
            this._btn_OutputParentFolder.Text = "...";
            this._btn_OutputParentFolder.UseVisualStyleBackColor = true;
            this._btn_OutputParentFolder.Click += new System.EventHandler(this._btn_OutputParentFolder_Click);
            // 
            // _txtBox_OutputParentFolder
            // 
            this._txtBox_OutputParentFolder.Location = new System.Drawing.Point(5, 122);
            this._txtBox_OutputParentFolder.Name = "_txtBox_OutputParentFolder";
            this._txtBox_OutputParentFolder.Size = new System.Drawing.Size(360, 20);
            this._txtBox_OutputParentFolder.TabIndex = 6;
            // 
            // _btn_TemporaryFolderLocation
            // 
            this._btn_TemporaryFolderLocation.Location = new System.Drawing.Point(367, 70);
            this._btn_TemporaryFolderLocation.Name = "_btn_TemporaryFolderLocation";
            this._btn_TemporaryFolderLocation.Size = new System.Drawing.Size(36, 23);
            this._btn_TemporaryFolderLocation.TabIndex = 4;
            this._btn_TemporaryFolderLocation.Text = "...";
            this._btn_TemporaryFolderLocation.UseVisualStyleBackColor = true;
            this._btn_TemporaryFolderLocation.Click += new System.EventHandler(this._btn_TemporaryFolderLocation_Click);
            // 
            // _txtBox_TemporaryFolerLocation
            // 
            this._txtBox_TemporaryFolerLocation.Location = new System.Drawing.Point(4, 71);
            this._txtBox_TemporaryFolerLocation.Name = "_txtBox_TemporaryFolerLocation";
            this._txtBox_TemporaryFolerLocation.Size = new System.Drawing.Size(360, 20);
            this._txtBox_TemporaryFolerLocation.TabIndex = 3;
            // 
            // _btn_searchFileToUpload
            // 
            this._btn_searchFileToUpload.AllowDrop = true;
            this._btn_searchFileToUpload.Location = new System.Drawing.Point(367, 32);
            this._btn_searchFileToUpload.Name = "_btn_searchFileToUpload";
            this._btn_searchFileToUpload.Size = new System.Drawing.Size(36, 23);
            this._btn_searchFileToUpload.TabIndex = 1;
            this._btn_searchFileToUpload.Text = "...";
            this._btn_searchFileToUpload.UseVisualStyleBackColor = false;
            this._btn_searchFileToUpload.Click += new System.EventHandler(this._btn_searchFileToUpload_Click);
            // 
            // _txtBox_FileToUpload
            // 
            this._txtBox_FileToUpload.Location = new System.Drawing.Point(7, 32);
            this._txtBox_FileToUpload.Name = "_txtBox_FileToUpload";
            this._txtBox_FileToUpload.Size = new System.Drawing.Size(360, 20);
            this._txtBox_FileToUpload.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._txtBox_OutputResult);
            this.groupBox2.Location = new System.Drawing.Point(447, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(363, 271);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output Result";
            // 
            // _txtBox_OutputResult
            // 
            this._txtBox_OutputResult.Location = new System.Drawing.Point(6, 24);
            this._txtBox_OutputResult.Multiline = true;
            this._txtBox_OutputResult.Name = "_txtBox_OutputResult";
            this._txtBox_OutputResult.Size = new System.Drawing.Size(351, 239);
            this._txtBox_OutputResult.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(32, 306);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(776, 38);
            this.button1.TabIndex = 2;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 372);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox _txtBox_CreateFolderName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _btn_StartImageProcesing;
        private System.Windows.Forms.Button _btn_OutputParentFolder;
        private System.Windows.Forms.TextBox _txtBox_OutputParentFolder;
        private System.Windows.Forms.Button _btn_TemporaryFolderLocation;
        private System.Windows.Forms.TextBox _txtBox_TemporaryFolerLocation;
        private System.Windows.Forms.Button _btn_searchFileToUpload;
        private System.Windows.Forms.TextBox _txtBox_FileToUpload;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _txtBox_OutputResult;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button1;
    }
}

