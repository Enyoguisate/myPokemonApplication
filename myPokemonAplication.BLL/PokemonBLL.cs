﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using myPokemonAplpication.DAL;
using myPokemonApp.Model;

namespace myPokemonAplication.BLL
{
    public class PokemonBLL
    {
        private PokemonDAL pokemonDAL;

        public PokemonBLL()
        {
            pokemonDAL = new PokemonDAL();
        }

        public List<Pokemon> GetAll()
        {
            return pokemonDAL.GetAllPokemon();
        }

        public List<Pokemon> GetById(int id)
        {
            return pokemonDAL.GetPokemonById(id);
        } 

        public bool CreatePokemon(Pokemon aPokemon)
        {
            bool aState = false;
            aState = pokemonDAL.InsertPokemon(aPokemon);
            return aState;
        }

        public bool UpdPokemon(Pokemon aPokemon)
        {
            return pokemonDAL.UpdPokemon(aPokemon);
        }

        public bool ValExist(Pokemon aPokemon)
        {
            bool aState = false;
            aState = pokemonDAL.ValExist(aPokemon);
            return aState;
        }
    }
}
