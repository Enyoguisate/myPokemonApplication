﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Web.Razor.Generator;
using myPokemonAplication.Models;
using Pokemon.Helper;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using myPokemonAplication.BLL;
using myPokemonApp.Model;
using PokemonType = myPokemonApp.Model.PokemonType;


namespace myPokemonAplication.Controllers
{
    public class AdminController : Controller
    {
        //  private PokemonBLL pokemonBll = new PokemonBLL();


        public ActionResult ABMPokemon()
        {
           myPokemonApp.Model.Pokemon objMock = new myPokemonApp.Model.Pokemon
           {
               //Id = 6,
               UrlImage = "/urlImage1",
               Type = PokemonType.Dark,
               PreviousEvolution = null,
               NextEvolution = 1,
               Name = "firstTest"
           };
            PokemonBLL objBll = new PokemonBLL();
            try
            {
               var ret = objBll.CreatePokemon(objMock);
            }
            catch (Exception)
            {
            }
            myPokemonApp.Model.Pokemon objMock2 = new myPokemonApp.Model.Pokemon
            {
                Id=2,
                Name="secondtest",
                UrlImage = "/urlimage2",
                Type = PokemonType.Electric,
                PreviousEvolution = 0,
                NextEvolution = 1
            };
            try
            {
                var ret = objBll.UpdPokemon(objMock2);
            }
            catch (Exception)
            {
            }

            return View();
        }

        //TODO Validate if return from BLL layer is null or no pokemon in the database.
        public ActionResult PokemonList()
        {
            PokemonBLL objBll = new PokemonBLL();
            
            return View(objBll.GetAll());
        }

        [HttpPost]
        public string UploadImage()
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var file = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];

                if (file != null && file.ContentLength > 0)
                    try
                    {
                        string pathTemporalFolder = Server.MapPath(ConfigurationManager.AppSettings["tempImagesFolder"]);
                        file.SaveAs(Path.Combine(pathTemporalFolder, file.FileName));
                        return file.FileName;
                    }
                    catch (Exception ex)
                    {
                        return "Error: " + ex.ToString();
                    }
                else
                {
                    ViewBag.Message = "You have not specified a file.";
                }
                return "UploadImageFileNotSelected";
            }
            return "UploadImageFileNotSelected";
        }

        [HttpPost]
        public ActionResult RenameImage(string filename)
        {
            string pathToFileInTemporalFolder =
                Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["tempImagesFolder"]), filename);
            System.IO.File.Move(pathToFileInTemporalFolder,
                Path.ChangeExtension(pathToFileInTemporalFolder, ".toDelete"));
            return Json(Url.Action("AbmPokemon", "Admin"));
        }


       
        [HttpGet]
        public ActionResult CreateNewUser()
        {
            var model = new UserModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateNewUser(UserModel model)
        {

            return View("CreateNewUser");
        }




        #region OLD CODE TO BE REMOVED

        //TODO REFACTOR THIS CODE 
        [HttpGet]
        public ActionResult AddPokemon()
        {
            var model = new PokemonModel();
            return View(model);
        }

        //TODO REFACTOR THIS CODE
        [HttpGet]
        public ActionResult UpdPokemon()
        {
            var model = new PokemonModel();
            return View(model);
        }


        [HttpPost]
        public ActionResult AddPokemon(PokemonModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ImageManager mgr = new ImageManager();

                    var fileNameWithExt = model.UrlImage;

                    if (!string.IsNullOrEmpty(fileNameWithExt))
                    {
                        var pathToprocessImages = Server.MapPath(ConfigurationManager.AppSettings["processImagesFolder"]);
                        var pathToTempImg = ConfigurationManager.AppSettings["tempImagesFolder"];
                        var pathFileWithExtension = Path.Combine(pathToTempImg, fileNameWithExt);
                        var finalPathWithExtension = Path.Combine(pathToTempImg,
                            Path.Combine(pathToTempImg, fileNameWithExt));
                        var finalPathToSaveImages = mgr.CreateFolderOfProcessPokemon(pathToprocessImages,
                            fileNameWithExt);
                        var img = new Bitmap(Server.MapPath(pathFileWithExtension));
                        mgr.SaveImg(mgr.ScaleImage(img, 75, 75), finalPathToSaveImages,
                            mgr.NameImage(finalPathWithExtension, 0));
                        mgr.SaveImg(mgr.ScaleImage(img, 150, 150), finalPathToSaveImages,
                            mgr.NameImage(finalPathWithExtension, 1));
                        mgr.SaveImg(img, finalPathToSaveImages, mgr.NameImage(finalPathWithExtension, 2));
                    }

                    myPokemonApp.Model.Pokemon poke = new myPokemonApp.Model.Pokemon();

                    poke.Id = model.Id;
                    poke.Name = model.Name;
                    poke.Type = (PokemonType)model.Type;
                    poke.UrlImage = model.UrlImage;
                    poke.PreviousEvolution = model.PreviousEvolution;
                    poke.NextEvolution = model.NextEvolution;

                    bool aState = false;
                    
                    PokemonBLL objBll = new PokemonBLL();
                    
                    //review if valExist it's really necesary
                    aState = objBll.ValExist(poke);

                    if (aState)
                    {
                        //return View("UpdPokemon");
                        objBll.UpdPokemon(poke);
                    }
                    else
                    {
                        objBll.CreatePokemon(poke);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Complete los siguientes campos:");
                    return View(model);
                }
            }
            catch (Exception)
            {
                return View(model);
            }

            ModelState.Clear();
            return View("AddPokemon");
        }





        //[HttpPost]
        //public ActionResult UpdPokemon(PokemonModel model)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            ImageManager mgr = new ImageManager();
        //            //mgr.ListenQueuer();
        //            var fileNameWithExt = model.UrlImage;
        //            if (!string.IsNullOrEmpty(fileNameWithExt))
        //            {
        //                var pathToprocessImages = Server.MapPath(ConfigurationManager.AppSettings["processImagesFolder"]);
        //                var pathToTempImg = ConfigurationManager.AppSettings["tempImagesFolder"];
        //                var pathFileWithExtension = Path.Combine(pathToTempImg, fileNameWithExt);
        //                var finalPathWithExtension = Path.Combine(pathToTempImg,
        //                    Path.Combine(pathToTempImg, fileNameWithExt));
        //                var finalPathToSaveImages = mgr.CreateFolderOfProcessPokemon(pathToprocessImages,
        //                    fileNameWithExt);
        //                var img = new Bitmap(Server.MapPath(pathFileWithExtension));
        //                mgr.SaveImg(mgr.ScaleImage(img, 75, 75), finalPathToSaveImages,
        //                    mgr.NameImage(finalPathWithExtension, 0));
        //                mgr.SaveImg(mgr.ScaleImage(img, 150, 150), finalPathToSaveImages,
        //                    mgr.NameImage(finalPathWithExtension, 1));
        //                mgr.SaveImg(img, finalPathToSaveImages, mgr.NameImage(finalPathWithExtension, 2));

        //            }
        //        }
        //        else
        //        {
        //            ModelState.AddModelError(string.Empty, "Complete los siguientes campos:");
        //            return View(model);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return View(model);
        //    }

        //    //myPokemonApp.Model.Pokemon poke = new myPokemonApp.Model.Pokemon();

        //    PokemonModel poke = new PokemonModel();
        //    myPokemonApp.Model.Pokemon pok2 = new myPokemonApp.Model.Pokemon();
        //    if (model.Id == 0)
        //    {
        //        pok2 = pokemonBll.GetPokemon(model.Id);
        //        poke.Id = pok2.Id;
        //        poke.Name = pok2.Name;
        //        poke.Type = (Models.PokemonType) pok2.Type;
        //        poke.UrlImage = pok2.UrlImage;
        //        poke.PreviousEvolution = pok2.PreviousEvolution;
        //        poke.NextEvolution = pok2.NextEvolution;
        //    }
        //    return View(poke);

        //    //return View("UpdPokemon");
        //}


        #endregion

    }


}




