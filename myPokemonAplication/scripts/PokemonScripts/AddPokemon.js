﻿var Mod = Mod || {};

Mod.AddPokemon = function() {
    $("#file").change(function () {
        var data = new FormData();
        var files = $("#file").get(0).files;
        if (files.length > 0) {
            data.append("HelpSectionImages", files[0]);
        }
        $.ajax({
            url: 'http://web.pokemoncalculator.com/Admin/UploadImage',
            data: data,
            processData: false, // Don't process the files
            contentType: false,
            type: 'POST',
            success: function(data) {
                $("#myModal").show();
                $("#UrlImage").val(data);
            },
            error: function(data) {
                $("#myModal").show();
            }
        });
        console.log(this.files[0]);
    });
    $("#cancel").click(function () {
        $.ajax({
            url: 'http://web.pokemoncalculator.com/Admin/RenameImage',
            data: { 'filename': $("#UrlImage").val() },
            type: "POST",
            success: function (data) {
                window.location.href = 'http://web.pokemoncalculator.com/' + data;
            },
            error: function () { }
        });
    });
}




