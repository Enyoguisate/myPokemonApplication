﻿using System;
using System.ComponentModel.DataAnnotations;

namespace myPokemonAplication.Models
{
    public class Login
    {
        [Required]
        [MaxLength(12,ErrorMessage ="Maximo permitido es 12")]
        public String UserName { get; set; }
        [Required]
        [MaxLength(15,ErrorMessage ="Maximo permitido es 15")]
        
        public String Password { get; set; }



    }
}