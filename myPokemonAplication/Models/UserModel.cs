﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace myPokemonAplication.Models
{
    public class UserModel
    {
        
        [DisplayName("User Name")]
        [Required(ErrorMessage = "The user name field is empty or invalid")]
        public string UserName { get; set; }
        [DisplayName("Password")]
        [Required(ErrorMessage = "The password field is empty or invalid")]
        public string Password { get; set; }
        [DisplayName("First Name")]
        [Required(ErrorMessage = "The first name field is empty or invalid")]
        public string Fname { get; set; }
        [DisplayName("Last Name")]
        [Required(ErrorMessage = "The last name field is empty or invalid")]
        public string Lname { get; set; }
        [DisplayName("Area Code")]
        [Required(ErrorMessage = "The Area Code field is empty or invalid")]
        public int AreaCode { get; set; }
        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "The phone number field is empty or invalid")]
        public Int64 Phone { get; set; }
        [DisplayName("Birth Day")]
        [Required(ErrorMessage = "The birth day field is empty or invalid")]
        public DateTime BirthDay { get; set; }
        [DisplayName("Email")]
        [Required(ErrorMessage = "The email field is empty or invalid")]
        public string Email { get; set; }
        [DisplayName("Trainer Level")]
        [Required(ErrorMessage = "The trainer level field is empty or invalid")]
        public int TrainerLvl { get; set; }
    }
}