﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FunScript.TypeScript;
using System.Data;
using myPokemonApp.Model;

namespace myPokemonAplication.Models
{
    public class PokemonModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name field is empty or invalid")]
        public string Name { get; set; }
        [Required]
        public PokemonType Type { get; set; }
        [Required(ErrorMessage = "Upload Image field is empty")]
        public string UrlImage { get; set; }
        public int PreviousEvolution { get; set; }
        public int NextEvolution { get; set; }
        
    }
    public enum PokemonType
    {
        Normal,
        Fire,
        Fighting,
        Water,
        Flying,
        Grass ,
        Poison ,
        Electric,
        Ground ,
        Psychic,
        Rock,
        Ice ,
        Bug ,
        Dragon,
        Ghost ,
        Dark ,
        Steel, 
        Fairy 

    }

      
}