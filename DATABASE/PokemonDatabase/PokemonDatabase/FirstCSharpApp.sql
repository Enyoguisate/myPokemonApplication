﻿USE [FirstCSharpApp]
GO
/****** Object:  Table [dbo].[PokemonByUser]    Script Date: 09/20/2016 17:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PokemonByUser](
	[idPokemon] [int] NOT NULL,
	[userName] [varchar](50) NOT NULL,
	[cpPokemon] [int] NULL,
	[hpPokemon] [int] NULL,
	[atkPokemon] [int] NULL,
	[defPokemon] [int] NULL,
	[staPokemon] [int] NULL,
	[dustPrice] [bigint] NULL,
	[poweredUp] [bit] NULL,
	[nroPokemon] [int] NOT NULL,
 CONSTRAINT [PK_PokemonByUser] PRIMARY KEY CLUSTERED 
(
	[userName] ASC,
	[nroPokemon] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pokemon]    Script Date: 09/20/2016 17:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pokemon](
	[id] [int] NOT NULL,
	[name] [varchar](100) NULL,
	[type] [varchar](50) NULL,
	[urlImage] [varchar](300) NULL,
	[previousEvolution] [int] NULL,
	[nextEvolution] [int] NULL,
 CONSTRAINT [PK_Pokemon] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 09/20/2016 17:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[userName] [varchar](50) NOT NULL,
	[password] [varchar](50) NULL,
	[fName] [varchar](100) NULL,
	[lName] [varchar](100) NULL,
	[areaCode] [int] NULL,
	[phone] [bigint] NULL,
	[birthDay] [datetime] NULL,
	[email] [varchar](100) NULL,
	[trainerLvl] [int] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[userName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_USERS]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to Update a user
-- =============================================
CREATE PROCEDURE [dbo].[SP_UPD_USERS]
	-- Add the parameters for the stored procedure here
	@userName varchar(50),
	@password varchar(100)=null,
	@fName varchar(100)=null,
	@lName varchar(100)=null,
	@areaCode int=null,
	@phone bigint=null,
	@birthDay datetime=null,
	@email varchar(100)=null,
	@trainerLvl int=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Users
	set
	[password]=ISNULL(@password,[password]),
	fName=ISNULL(@fName,fName),
	lName=ISNULL(@lName,lName),
	areaCode=ISNULL(@areaCode,areaCode),
	phone=ISNULL(@phone,phone),
	birthDay=ISNULL(@birthDay,birthDay),
	email=ISNULL(@email,email),
	trainerLvl=ISNULL(@trainerLvl,trainerLvl)
	from Users
	where
	userName=@userName

END
GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_POKEMONBYUSER]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to update a pokemon by user
-- =============================================
CREATE PROCEDURE [dbo].[SP_UPD_POKEMONBYUSER]
	-- Add the parameters for the stored procedure here
	@idPokemon int=null,
	@userName varchar(50),
	@cpPokemon int=null,
	@hpPokemon int=null,
	@atkPokemon int=null,
	@defPokemon int=null,
	@staPokemon int=null,
	@dustPrice bigint=null,
	@poweredUp bit=null,
	@nroPokemon int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE PokemonByUser
	set
		--idPokemon=ISNULL(@idPokemon,idPokemon),
		userName=ISNULL(@userName,userName),
		cpPokemon=ISNULL(@cpPokemon,cpPokemon),
		hpPokemon=ISNULL(@hpPokemon,hpPokemon),
		atkPokemon=ISNULL(@atkPokemon,atkPokemon),
		defPokemon=ISNULL(@defPokemon,defPokemon),
		staPokemon=ISNULL(@staPokemon,staPokemon),
		dustPrice=ISNULL(@dustPrice,dustPrice),
		poweredUp=ISNULL(@poweredUp,poweredUp)
		from PokemonByUser
		where
		(idPokemon=@idPokemon)
		or
		(nroPokemon=@nroPokemon)

END
GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_POKEMON]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to update a pokemon 
-- =============================================
CREATE PROCEDURE [dbo].[SP_UPD_POKEMON]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(100)=null,
	@type varchar(50)=null,
	@urlImage varchar(300)=null,
	@previousEvolution int=null,
	@nextEvolution int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Pokemon
	set
	name = ISNULL(@name,name),
	[type]=ISNULL(@type,[type]),
	urlImage=ISNULL(@urlImage,urlImage),
	previousEvolution=ISNULL(@previousEvolution,previousEvolution),
	nextEvolution=ISNULL(@nextEvolution,nextEvolution)
    -- Insert statements for procedure here
	from pokemon
	where
	id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INS_USERS]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP inserts a new User
-- =============================================
CREATE PROCEDURE [dbo].[SP_INS_USERS]
	-- Add the parameters for the stored procedure here
	@userName varchar(50),
	@password varchar(50) = null,
	@fName varchar(100) = null,
	@lName varchar(100) = null,
	@areaCode int = null,
	@phone bigint = null,
	@birthDay datetime = null,
	@email varchar(100) = null,
	@trainerLvl int = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Users (userName,[password],fName,lName,areaCode,phone,birthDay,email,trainerLvl)
	values (@userName,@password,@fName,@lName,@areaCode,@phone,@birthDay,@email,@trainerLvl)
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INS_POKEMONBYUSER]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to insert Pokemon's by user
-- =============================================
CREATE PROCEDURE [dbo].[SP_INS_POKEMONBYUSER]
	-- Add the parameters for the stored procedure here
	@idPokemon int=null,
	@userName varchar(50),
	@cpPokemon int = null,
	@hpPokemon int = null,
	@atkPokemon int = null,
	@defPokemon int = null,
	@staPokemon int = null,
	@dustPrice bigint = null,
	@poweredUp bit = null,
	@nroPokemon int
	
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into PokemonByUser (idPokemon,userName,cpPokemon,hpPokemon,atkPokemon,defPokemon,staPokemon,dustPrice,poweredUp,nroPokemon)
	values(@idPokemon,@userName,@cpPokemon,@hpPokemon,@atkPokemon,@defPokemon,@staPokemon,@dustPrice,@poweredUp,@nroPokemon)
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INS_POKEMON]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	SP to insert Pokemon
-- =============================================
CREATE PROCEDURE [dbo].[SP_INS_POKEMON]
	-- Add the parameters for the stored procedure here
	@id int = null,
	@name varchar(100) = null,
	@type varchar(50) = null,
	@urlImage varchar(300) = null,
	@previousEvolution int = null,
	@nextEvolution int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Pokemon (id,name,[type],urlImage,previousEvolution,nextEvolution)
	values(@id,@name,@type,@urlImage,@previousEvolution,@nextEvolution)
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_USERS_BY_USERNAME_OR_EMAIL]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to get a user
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_USERS_BY_USERNAME_OR_EMAIL]
	-- Add the parameters for the stored procedure here
	@userName varchar(50)=null,
	@email varchar(100)=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT userName,[password],fName,lName,areaCode,phone,birthDay,email,trainerLvl
	from Users
	where
	(@userName is null or @userName=userName)
	and 
	(@email is null or @email=email)
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_POKEMONBYUSER_BY_ID]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to get pokemons by user
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_POKEMONBYUSER_BY_ID] 
	-- Add the parameters for the stored procedure here
	@idPokemon int = null,
	@userName varchar(50) = null,
	@nroPokemon int = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT idPokemon,userName,cpPokemon,hpPokemon,atkPokemon,defPokemon,staPokemon,dustPrice,poweredUp,nroPokemon
	from PokemonByUser
	where
	(@idPokemon is null or @idPokemon=idPokemon)
	and 
	(@userName IS null or @userName=userName)
	and
	(@nroPokemon is null or @nroPokemon=nroPokemon)
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_POKEMON_BY_ID]    Script Date: 09/20/2016 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to selec a pokemon
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_POKEMON_BY_ID] 
	-- Add the parameters for the stored procedure here
	@id int = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id,name,[type],urlImage,previousEvolution,nextEvolution
	from Pokemon
	where
	(@id is null or @id=id)
END
GO
