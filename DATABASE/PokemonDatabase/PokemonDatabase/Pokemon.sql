﻿CREATE TABLE [dbo].[Pokemon] (
    [id]                INT           NOT NULL,
    [name]              VARCHAR (100) NULL,
    [type]              VARCHAR (50)  NULL,
    [urlImage]          VARCHAR (300) NULL,
    [previousEvolution] INT           NULL,
    [nextEvolution]     INT           NULL,
    CONSTRAINT [PK_Pokemon] PRIMARY KEY CLUSTERED ([id] ASC)
);

