﻿CREATE TABLE [dbo].[PokemonByUser] (
    [idPokemon]  INT          NOT NULL,
    [userName]   VARCHAR (50) NOT NULL,
    [cpPokemon]  INT          NULL,
    [hpPokemon]  INT          NULL,
    [atkPokemon] INT          NULL,
    [defPokemon] INT          NULL,
    [staPokemon] INT          NULL,
    [dustPrice]  BIGINT       NULL,
    [poweredUp]  BIT          NULL,
    [nroPokemon] INT          NOT NULL,
    CONSTRAINT [PK_PokemonByUser] PRIMARY KEY CLUSTERED ([userName] ASC, [nroPokemon] ASC)
);

