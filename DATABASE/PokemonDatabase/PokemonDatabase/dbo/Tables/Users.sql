﻿CREATE TABLE [dbo].[Users] (
    [userName]   VARCHAR (50)  NOT NULL,
    [password]   VARCHAR (50)  NULL,
    [fName]      VARCHAR (100) NULL,
    [lName]      VARCHAR (100) NULL,
    [areaCode]   INT           NULL,
    [phone]      BIGINT        NULL,
    [birthDay]   DATETIME      NULL,
    [email]      VARCHAR (100) NULL,
    [trainerLvl] INT           NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([userName] ASC)
);

