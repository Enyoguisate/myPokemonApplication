﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to update a pokemon by user
-- =============================================
CREATE PROCEDURE SP_UPD_POKEMONBYUSER
	-- Add the parameters for the stored procedure here
	@idPokemon int=null,
	@userName varchar(50),
	@cpPokemon int=null,
	@hpPokemon int=null,
	@atkPokemon int=null,
	@defPokemon int=null,
	@staPokemon int=null,
	@dustPrice bigint=null,
	@poweredUp bit=null,
	@nroPokemon int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE PokemonByUser
	set
		--idPokemon=ISNULL(@idPokemon,idPokemon),
		userName=ISNULL(@userName,userName),
		cpPokemon=ISNULL(@cpPokemon,cpPokemon),
		hpPokemon=ISNULL(@hpPokemon,hpPokemon),
		atkPokemon=ISNULL(@atkPokemon,atkPokemon),
		defPokemon=ISNULL(@defPokemon,defPokemon),
		staPokemon=ISNULL(@staPokemon,staPokemon),
		dustPrice=ISNULL(@dustPrice,dustPrice),
		poweredUp=ISNULL(@poweredUp,poweredUp)
		from PokemonByUser
		where
		(idPokemon=@idPokemon)
		or
		(nroPokemon=@nroPokemon)

END