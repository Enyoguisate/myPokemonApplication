﻿-- =============================================
-- Author:		<Fernandez Ignacio>
-- Create date: <09/17/2016>
-- Description:	<Get user based On userName and Password>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_Login]
	-- Add the parameters for the stored procedure here
	@Username varchar(100),
	@Password varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Users WHERE userName = @Username AND [password] = @Password
END