﻿-- =============================================
-- Author:		<Fernandez Ignacio>
-- Create date: <09/24/2016>
-- Description:	<Check availability of a username or email>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_User_Availability]
	-- Add the parameters for the stored procedure here
	@Username varchar(50)= null,
	@Email  varchar(100)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS
    (
    SELECT *
    FROM Users
    WHERE (@Username IS NULL or userName = @Username)
        AND
          (@Email IS NULL or email = @Email)
    )
    BEGIN
        SELECT 0
    END
ELSE
    BEGIN
        SELECT 1
    END

END