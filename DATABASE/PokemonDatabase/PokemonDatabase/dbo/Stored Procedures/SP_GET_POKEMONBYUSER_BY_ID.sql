﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to get pokemons by user
-- =============================================
CREATE PROCEDURE SP_GET_POKEMONBYUSER_BY_ID 
	-- Add the parameters for the stored procedure here
	@idPokemon int = null,
	@userName varchar(50) = null,
	@nroPokemon int = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT idPokemon,userName,cpPokemon,hpPokemon,atkPokemon,defPokemon,staPokemon,dustPrice,poweredUp,nroPokemon
	from PokemonByUser
	where
	(@idPokemon is null or @idPokemon=idPokemon)
	and 
	(@userName IS null or @userName=userName)
	and
	(@nroPokemon is null or @nroPokemon=nroPokemon)
END