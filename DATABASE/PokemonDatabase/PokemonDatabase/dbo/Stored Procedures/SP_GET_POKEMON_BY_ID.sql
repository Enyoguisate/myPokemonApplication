﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to selec a pokemon
-- =============================================
CREATE PROCEDURE SP_GET_POKEMON_BY_ID 
	-- Add the parameters for the stored procedure here
	@id int = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id,name,[type],urlImage,previousEvolution,nextEvolution
	from Pokemon
	where
	(@id is null or @id=id)
END