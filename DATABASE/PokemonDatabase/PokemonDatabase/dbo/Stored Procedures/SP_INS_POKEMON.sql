﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	SP to insert Pokemon
-- =============================================
CREATE PROCEDURE SP_INS_POKEMON
	-- Add the parameters for the stored procedure here
	@id int = null,
	@name varchar(100) = null,
	@type varchar(50) = null,
	@urlImage varchar(300) = null,
	@previousEvolution int = null,
	@nextEvolution int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Pokemon (id,name,[type],urlImage,previousEvolution,nextEvolution)
	values(@id,@name,@type,@urlImage,@previousEvolution,@nextEvolution)
	
END