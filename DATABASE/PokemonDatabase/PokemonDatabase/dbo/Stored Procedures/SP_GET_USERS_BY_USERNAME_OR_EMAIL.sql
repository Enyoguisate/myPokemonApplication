﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to get a user
-- =============================================
CREATE PROCEDURE SP_GET_USERS_BY_USERNAME_OR_EMAIL
	-- Add the parameters for the stored procedure here
	@userName varchar(50)=null,
	@email varchar(100)=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT userName,[password],fName,lName,areaCode,phone,birthDay,email,trainerLvl
	from Users
	where
	(@userName is null or @userName=userName)
	and 
	(@email is null or @email=email)
END