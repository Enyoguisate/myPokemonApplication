﻿
-- =============================================
-- Author:		<Fernandez Ignacio>
-- Create date: <09/17/2016>
-- Description:	<Get user based On userName and Password>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_User]
	-- Add the parameters for the stored procedure here
	@Username varchar(100)=null,
	@Email varchar(100)= null,
	@Fname varchar(100)=null,
	@Lname varchar(100)=null,
	@RolId int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Users 
	WHERE 
	(@Username IS NULL OR userName = @Username)
	 AND 
	(@Email IS NULL OR email = @Email)
	AND
	(@Fname IS NULL OR fName = @Fname)
	AND 
	(@Lname IS NULL OR lName = @Lname)
	
END