﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to insert Pokemon's by user
-- =============================================
CREATE PROCEDURE [dbo].[SP_INS_POKEMONBYUSER]
	-- Add the parameters for the stored procedure here
	@idPokemon int=null,
	@userName varchar(50),
	@cpPokemon int = null,
	@hpPokemon int = null,
	@atkPokemon int = null,
	@defPokemon int = null,
	@staPokemon int = null,
	@dustPrice bigint = null,
	@poweredUp bit = null,
	@nroPokemon int
	
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into PokemonByUser (idPokemon,userName,cpPokemon,hpPokemon,atkPokemon,defPokemon,staPokemon,dustPrice,poweredUp,nroPokemon)
	values(@idPokemon,@userName,@cpPokemon,@hpPokemon,@atkPokemon,@defPokemon,@staPokemon,@dustPrice,@poweredUp,@nroPokemon)
	
END