﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP inserts a new User
-- =============================================
CREATE PROCEDURE [dbo].[SP_INS_USERS]
	-- Add the parameters for the stored procedure here
	@userName varchar(50),
	@password varchar(50) = null,
	@fName varchar(100) = null,
	@lName varchar(100) = null,
	@areaCode int = null,
	@phone bigint = null,
	@birthDay datetime = null,
	@email varchar(100) = null,
	@trainerLvl int = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Users (userName,[password],fName,lName,areaCode,phone,birthDay,email,trainerLvl)
	values (@userName,@password,@fName,@lName,@areaCode,@phone,@birthDay,@email,@trainerLvl)
END