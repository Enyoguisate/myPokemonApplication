﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to update a pokemon 
-- =============================================
CREATE PROCEDURE SP_UPD_POKEMON
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(100)=null,
	@type varchar(50)=null,
	@urlImage varchar(300)=null,
	@previousEvolution int=null,
	@nextEvolution int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Pokemon
	set
	name = ISNULL(@name,name),
	[type]=ISNULL(@type,[type]),
	urlImage=ISNULL(@urlImage,urlImage),
	previousEvolution=ISNULL(@previousEvolution,previousEvolution),
	nextEvolution=ISNULL(@nextEvolution,nextEvolution)
    -- Insert statements for procedure here
	from pokemon
	where
	id=@id
END