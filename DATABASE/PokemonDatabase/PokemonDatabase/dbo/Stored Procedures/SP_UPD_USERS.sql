﻿-- =============================================
-- Author:		Victor Martin
-- Create date: 06/09/2016
-- Description:	This SP is use to Update a user
-- =============================================
CREATE PROCEDURE SP_UPD_USERS
	-- Add the parameters for the stored procedure here
	@userName varchar(50),
	@password varchar(100)=null,
	@fName varchar(100)=null,
	@lName varchar(100)=null,
	@areaCode int=null,
	@phone bigint=null,
	@birthDay datetime=null,
	@email varchar(100)=null,
	@trainerLvl int=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Users
	set
	[password]=ISNULL(@password,[password]),
	fName=ISNULL(@fName,fName),
	lName=ISNULL(@lName,lName),
	areaCode=ISNULL(@areaCode,areaCode),
	phone=ISNULL(@phone,phone),
	birthDay=ISNULL(@birthDay,birthDay),
	email=ISNULL(@email,email),
	trainerLvl=ISNULL(@trainerLvl,trainerLvl)
	from Users
	where
	userName=@userName

END